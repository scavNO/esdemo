# Event sourced Demo #

*My first attempt*

Goal: Eventstore saved to and replayed from mongodb using kryo.

- This application is built around the idea that all that should be explained.
- No event is to small to be added as a unique event. Even if it feels like it can or should be omitted.

## Running ##
The tests should be self contained and run the application basic functionality without any problem.
A basic serialized eventstore is included with the application.

To run use **debugpersisted.sh** and set up remote debug in IntelliJ.
To run the web client cd to ESDClient  and issue **gulp serve** 

