'use strict';

var AppDirectives = angular.module('ESDClient.directives', []);

AppDirectives.directive('appVersion', ['version', function (version) {
    return function (scope, elm, attrs) {
        elm.text(version);
    };
}]);

/**
 * Simple directive to provide moment.js formating
 * of the dates represented.
 *
 * NOTICE: This directive should be used in any input
 * fields where the date is being sent back to the server. *
 **/
AppDirectives.directive('formatDate', function() {
    return {
        scope: { date: '=date'},

        link: function (scope, element) {
            $(function () {
                if (scope.date) {
                    element.append(moment(scope.date).format('MM.DD.YYYY HH:mm'));
                }
            });
        }
    }
});