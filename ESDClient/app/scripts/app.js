'use strict';

var ESDClient = angular.module('ESDClient',
    ['ngRoute',
        'ngResource',
        'ESDClient.filters',
        'ESDClient.services',
        'ESDClient.directives']);

ESDClient.config(['$routeProvider', function ($routeProvider) {

    $routeProvider.when('/', {
        templateUrl: 'partials/main.html',
        controller: MainController
    });

    $routeProvider.when('/customer', {
        templateUrl: 'partials/customer.html',
        controller: CustomerController
    });

    $routeProvider.otherwise({
        redirectTo: '/partials/main.html',
    });
}]);