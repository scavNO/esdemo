'use strict';

var CustomerController = function($scope, $http, $resource) {

    $scope.newEmailData = {};
    $scope.newNameData = {};

    $scope.fetchCustomers = function() {
        $http.get('http://localhost:8080/customer/list').success(function(customers){
            $scope.customers = customers;
        });
    };
    $scope.fetchCustomers();

    $scope.getCustomerLog = function(customerId) {
        $http.get('http://localhost:8080/customer/history/'+customerId).success(function(customerLog) {
            $scope.customerLog = customerLog;
            $scope.newEmailData.customerId = customerId;
            $scope.newNameData.customerId = customerId;
        })
    };


    var newEmail = $resource(
        'http://localhost:8080/customer/email/:customerId/:newEmail', {});

    $scope.newEmailEvent = function() {
        $scope.participant = {};
        $scope.participant = newEmail.get($scope.newEmailData).$promise.then(function(result) {
        });
    };

    var newName = $resource(
        'http://localhost:8080/customer/name/:customerId/:newName', {});

    $scope.newNameEvent = function() {
        $scope.participant = {};
        $scope.participant = newName.get($scope.newNameData).$promise.then(function(result) {
        });
    };
};