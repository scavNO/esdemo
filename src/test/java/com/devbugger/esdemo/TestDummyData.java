package com.devbugger.esdemo;

import com.devbugger.esdemo.core.CoreService;
import com.devbugger.esdemo.customer.*;
import com.devbugger.esdemo.core.Event;
import com.devbugger.esdemo.core.EventStore;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ESDemo.class)
public class TestDummyData {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private CoreService coreService;

    @Before
    public void setUp() {
        SetupESDemo setupESDemo = new SetupESDemo(coreService, customerService);
        setupESDemo.initializeDummeEventStore();
    }

    @Test
    public void testSerialzeEventStore() throws Exception {
        coreService.serializeEventStore();
    }

    @Test
    public void testInitializeEventStore() throws Exception {
        coreService.initEventStore();
    }
}