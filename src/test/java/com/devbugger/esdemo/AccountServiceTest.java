package com.devbugger.esdemo;

import com.devbugger.esdemo.account.*;
import com.devbugger.esdemo.core.AggregateId;
import com.devbugger.esdemo.core.Event;
import com.devbugger.esdemo.core.EventStore;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ESDemo.class)
@ActiveProfiles(ESDemoProfile.DUMMY)
public class AccountServiceTest {

    @Autowired
    private AccountService accountService;

    @Autowired
    private AccountProjection accountProjection;

    @Autowired
    private EventStore eventStore;

    private AccountId accountId;

    @Before
    public void setUp() {
        accountId = new AccountId(AggregateId.create());
    }

    @Test
    public void testCreateAccount() throws Exception {
        accountService.createAccount(accountId);
        Account account = accountProjection.getAccount(accountId);

        assert account.getAggregateId() != null;
    }

    @Test
    public void testAccountBalance() throws Exception {
        accountService.createAccount(accountId);
        accountService.deposit(accountId, 200);
        accountService.withdraw(accountId, 150);
        accountService.deposit(accountId, 500);
        accountService.deposit(accountId, 100);
        accountService.withdraw(accountId, 250);

        Account account = accountProjection.getAccount(accountId);
        assert account.getBalance() == 400;
    }

    @Test
    public void testAccountEvents() throws Exception {
        accountService.createAccount(accountId); //0
        accountService.deposit(accountId, 200);  //1
        accountService.withdraw(accountId, 150); //2
        accountService.deposit(accountId, 500);  //3
        accountService.deposit(accountId, 100);  //4
        accountService.withdraw(accountId, 250); //5

        List<Event> events = eventStore.getEventsByAggregateId(accountId);        
        assert events.get(0) instanceof AccountCreated;
        assert events.get(1) instanceof AccountDeposited;
        assert events.get(2) instanceof AccountWithdrawed;
        assert events.get(3) instanceof AccountDeposited;
        assert events.get(4) instanceof AccountDeposited;
        assert events.get(5) instanceof AccountWithdrawed;
    }

    @Test
    public void testFindAccountByCustomer() throws Exception {
        //Implement.
    }
}