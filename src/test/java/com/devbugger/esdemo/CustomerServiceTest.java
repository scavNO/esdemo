package com.devbugger.esdemo;

import com.devbugger.esdemo.core.Event;
import com.devbugger.esdemo.core.EventStore;
import com.devbugger.esdemo.customer.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.UUID;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ESDemo.class)
@ActiveProfiles(ESDemoProfile.DUMMY)
public class CustomerServiceTest {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private CustomerProjection customerProjection;

    @Autowired
    private EventStore eventStore;

    @Test
    public void testGetAllCustomers() throws Exception {
        List<Customer> customers = customerProjection.getAllCustomers();
        customers.forEach((customer) -> {
            assert customer.getAggregateId() != null;
        });
    }

    @Test
    public void testCreateCustomer() throws Exception {
        CustomerId id = new CustomerId(UUID.randomUUID().toString());
        customerService.createCustomer(id);
        Customer customer = customerProjection.getCustomer(id);
        assert customer.getAggregateId() != null;
    }

    @Test
    public void testCustomerCreateEmail() throws Exception {
        CustomerId id = new CustomerId(UUID.randomUUID().toString());
        customerService.createCustomer(id);
        customerService.changeEmail(id, "testcustomer@gmail.com");
        Customer customer = customerProjection.getCustomer(id);

        assert customer.getEmail().equals("testcustomer@gmail.com");
    }

    @Test
    public void testCustomerChangeEmailAndName() throws Exception {
        CustomerId id = new CustomerId(UUID.randomUUID().toString());
        customerService.createCustomer(id);
        customerService.changeEmail(id, "tester@gmail.com");
        customerService.changeName(id, "Test Testesen");

        Customer customer = customerProjection.getCustomer(id);
        assert customer.getEmail().equals("tester@gmail.com");
        assert customer.getName().equals("Test Testesen");
    }

    @Test
    public void testCustomerEvents() throws Exception {
        CustomerId id = new CustomerId(UUID.randomUUID().toString());
        customerService.createCustomer(id);                     //0
        customerService.changeName(id, "Test Exampleman");      //1
        customerService.changeEmail(id, "testman@example.org"); //2

        List<Event> events = eventStore.getEventsByAggregateId(id);
        assert events.get(0) instanceof CustomerCreated;
        assert events.get(1) instanceof CustomerNameChanged;
        assert events.get(2) instanceof CustomerEmailChanged;
    }


}
