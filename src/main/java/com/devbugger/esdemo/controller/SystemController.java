package com.devbugger.esdemo.controller;

import com.devbugger.esdemo.core.CoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("system")
public class SystemController {

    @Autowired
    private CoreService coreService;

    @RequestMapping(value = "persist", method = RequestMethod.GET)
    public String persistEventStore() {
        coreService.serializeEventStore();

        return "Persisted eventstore to disk.";
    }
}
