package com.devbugger.esdemo.controller;

import com.devbugger.esdemo.account.Account;
import com.devbugger.esdemo.account.AccountId;
import com.devbugger.esdemo.account.AccountProjection;
import com.devbugger.esdemo.account.AccountService;
import com.devbugger.esdemo.core.AggregateType;
import com.devbugger.esdemo.core.Event;
import com.devbugger.esdemo.core.EventStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("account")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @Autowired
    private AccountProjection accountProjection;

    @Autowired
    private EventStore eventStore;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public List<Account> list() {
        return accountProjection.getAllAccounts();
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public Account createAccount() {
        AccountId id = new AccountId(UUID.randomUUID().toString());
        accountService.createAccount(id);

        return accountProjection.getAccount(id);
    }

    @RequestMapping(value = "deposit/{id}/{amount}", method = RequestMethod.POST)
    public Account deposit(@PathVariable("id") AccountId id, @PathVariable("amount") long amount) {
        accountService.deposit(id, amount);

        return accountProjection.getAccount(id);
    }

    @RequestMapping(value = "withdraw/{id}/{amount}", method = RequestMethod.POST)
    public Account withdraw(@PathVariable("id") AccountId id, @PathVariable("amount") long amount) {
        accountService.withdraw(id, amount);

        return accountProjection.getAccount(id);
    }

    @RequestMapping(value = "history/{id}", method = RequestMethod.GET)
    public List<Event> accountEventHistory(@PathVariable("id") AccountId id) {
        return eventStore.getEventsByAggregateId(id);
    }

    @RequestMapping(value = "history", method = RequestMethod.GET)
    public List<Event> accountEventAllHistory() {
        return eventStore.getEventsByAggregateType(AggregateType.ACCOUNT);
    }

}
