package com.devbugger.esdemo.controller;

import com.devbugger.esdemo.customer.Customer;
import com.devbugger.esdemo.customer.CustomerId;
import com.devbugger.esdemo.customer.CustomerProjection;
import com.devbugger.esdemo.customer.CustomerService;
import com.devbugger.esdemo.core.AggregateType;
import com.devbugger.esdemo.core.Event;
import com.devbugger.esdemo.core.EventStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private CustomerProjection customerProjection;

    @Autowired
    private EventStore eventStore;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public List<Customer> list() {
        return customerProjection.getAllCustomers();
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public Customer add() {
        CustomerId id = new CustomerId(UUID.randomUUID().toString());
        customerService.createCustomer(id);

        return customerProjection.getCustomer(id);
    }

    @RequestMapping(value = "email/{id}/{newMail}", method = RequestMethod.GET)
    public Customer updateEmail(@PathVariable("id") CustomerId id, @PathVariable("newMail") String newMail) {
        customerService.changeEmail(id, newMail);

        return customerProjection.getCustomer(id);
    }

    @RequestMapping(value = "name/{id}/{newName}", method = RequestMethod.GET)
    public Customer updateName(@PathVariable("id") CustomerId id, @PathVariable("newName") String newName) {
        customerService.changeName(id, newName);

        return customerProjection.getCustomer(id);
    }

    @RequestMapping(value = "history/{id}", method = RequestMethod.GET)
    public List<Event> customerEventHistory(@PathVariable("id") CustomerId id) {
        return eventStore.getEventsByAggregateId(id);
    }

    @RequestMapping(value = "history", method = RequestMethod.GET)
    public List<Event> customerEventAllHistory() {
        return eventStore.getEventsByAggregateType(AggregateType.CUSTOMER);
    }
}
