package com.devbugger.esdemo.config;

import com.devbugger.esdemo.account.AccountProjection;
import com.devbugger.esdemo.core.EventStore;
import com.devbugger.esdemo.customer.CustomerProjection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProjectionBeans {

    @Autowired
    private EventStore eventStore;

    @Bean
    public CustomerProjection customerProjection() {
        return new CustomerProjection(eventStore);
    }

    @Bean
    public AccountProjection accountProjection() {
        return new AccountProjection(eventStore);
    }
}