package com.devbugger.esdemo.customer;


import com.devbugger.esdemo.core.Event;
import com.devbugger.esdemo.core.EventStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Service("customerService")
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private EventStore eventStore;

    @Override
    public void createCustomer(CustomerId id) {
        List<Event> events = eventStore.getEventsByAggregateId(id);
        CustomerAggregate customerAggregate = new CustomerAggregate(events);
        customerAggregate.createCustomer(id);

        List<Event> derivedEvents = customerAggregate.getDerivedEvents();
        eventStore.storeEvents(derivedEvents);
    }

    public void changeEmail(CustomerId id, String newEmail) {
        List<Event> events = eventStore.getEventsByAggregateId(id);
        CustomerAggregate customerAggregate = new CustomerAggregate(events);
        customerAggregate.changeEmail(newEmail);

        List<Event> derivedEvents = customerAggregate.getDerivedEvents();
        eventStore.storeEvents(derivedEvents);
    }

    public void changeName(CustomerId id, String newName) {
        List<Event> events = eventStore.getEventsByAggregateId(id);
        CustomerAggregate customerAggregate = new CustomerAggregate(events);
        customerAggregate.changeName(newName);

        List<Event> derivedEvents = customerAggregate.getDerivedEvents();
        eventStore.storeEvents(derivedEvents);
    }
}