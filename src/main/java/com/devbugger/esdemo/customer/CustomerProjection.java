package com.devbugger.esdemo.customer;

import com.devbugger.esdemo.core.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomerProjection extends Projection {

    private Map<AggregateId, Customer> customers = new HashMap<>();

    public CustomerProjection(EventStore eventStore) {
        super(eventStore);
    }

    @Override
    protected AggregateType getAggregateType() {
        return AggregateType.CUSTOMER;
    }

    @Override
    public void handleEvent(Event event) {
        if(event instanceof CustomerCreated) {
            createCustomer((CustomerCreated) event);
        }

        else if(event instanceof CustomerNameChanged) {
            changeName((CustomerNameChanged)event);
        }

        else if(event instanceof CustomerEmailChanged) {
            changeEmail((CustomerEmailChanged)event);
        }
    }

    private void createCustomer(CustomerCreated event) {
        if(customers.containsKey(event.getAggregateId())) {
            System.err.println("Already contains this customer!");
        }

        else {
            customers.put(event.getAggregateId(), new Customer(event.getAggregateId()));
        }
    }

    private void changeName(CustomerNameChanged event) {
        Customer customer = customers.get(event.getAggregateId());
        customer.setName(event.getNewName());
        customers.put(event.getAggregateId(), customer);
    }

    private void changeEmail(CustomerEmailChanged event) {
        Customer customer = customers.get(event.getAggregateId());
        customer.setEmail(event.getNewEmail());
        customers.put(event.getAggregateId(), customer);
    }

    public Customer getCustomer(CustomerId customerId) {
        return customers.get(customerId);
    }

    public List<Customer> getAllCustomers() {
        return new ArrayList<>(customers.values());
    }
}