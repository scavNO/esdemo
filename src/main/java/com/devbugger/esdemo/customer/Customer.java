package com.devbugger.esdemo.customer;

import com.devbugger.esdemo.core.AggregateId;

/**
 * Customer Domain Object.
 * Is populated from the events found in {@link com.devbugger.esdemo.core.EventStore}
 */
public class Customer {

    private AggregateId aggregateId;

    private String name;

    private String email;

    public Customer(CustomerId customerId) {
        this.aggregateId = customerId;
    }

    public AggregateId getAggregateId() {
        return aggregateId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "CustomerInfo{" +
                "aggregateId=" + aggregateId +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}