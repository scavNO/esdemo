package com.devbugger.esdemo.customer;

import com.devbugger.esdemo.core.AggregateType;
import com.devbugger.esdemo.core.Event;

import java.time.Instant;

public class CustomerEvent extends Event {

    private final CustomerId customerId;

    private final String occurred;

    public CustomerEvent(CustomerId customerId) {
        this.customerId = customerId;
        this.occurred = Instant.now().toString();
    }

    @Override
    public String getOccurred() {
        return occurred;
    }

    @Override
    public CustomerId getAggregateId() {
        return customerId;
    }

    @Override
    public AggregateType getAggregateType() {
        return AggregateType.CUSTOMER;
    }
}
