package com.devbugger.esdemo.customer;

public class CustomerNameChanged extends CustomerEvent {

    private final String newName;

    public CustomerNameChanged(CustomerId id, String newName) {
        super(id);
        this.newName = newName;
    }

    public String getNewName() {
        return newName;
    }
}
