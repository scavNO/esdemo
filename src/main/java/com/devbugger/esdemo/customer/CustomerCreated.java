package com.devbugger.esdemo.customer;

public class CustomerCreated extends CustomerEvent {

    public CustomerCreated(CustomerId id) {
        super(id);
    }
}
