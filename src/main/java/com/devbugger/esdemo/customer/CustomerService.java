package com.devbugger.esdemo.customer;

public interface CustomerService {

    public void createCustomer(CustomerId id);

    public void changeEmail(CustomerId id, String newEmail);

    public void changeName(CustomerId id, String newName);
}