package com.devbugger.esdemo.customer;

public class CustomerEmailChanged extends CustomerEvent {

    private final String newEmail;

    public CustomerEmailChanged(CustomerId customerId, String newEmail) {
        super(customerId);
        this.newEmail = newEmail;
    }

    public String getNewEmail() {
        return newEmail;
    }

}