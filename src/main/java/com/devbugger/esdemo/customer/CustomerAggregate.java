package com.devbugger.esdemo.customer;

import com.devbugger.esdemo.core.Event;

import java.util.ArrayList;
import java.util.List;

public class CustomerAggregate {

    private CustomerState customerState;
    private List<Event> derivedEvents = new ArrayList<>();

    public CustomerAggregate(List<Event> events) {
        events.forEach((event) -> {
            if(event instanceof CustomerCreated) {
                customerState = new CustomerState((CustomerId)event.getAggregateId());
            }

            else if(event instanceof CustomerNameChanged) {
                customerState = new CustomerState((CustomerId)event.getAggregateId());
            }

            else if(event instanceof CustomerEmailChanged) {
                customerState = new CustomerState((CustomerId)event.getAggregateId());
            }
        });
    }

    public void createCustomer(CustomerId id) {
        CustomerCreated customerCreated = new CustomerCreated(id);
        customerState = new CustomerState((customerCreated).getAggregateId());
        derivedEvents.add(customerCreated);

    }

    public void changeName(String newName) {
        derivedEvents.add(new CustomerNameChanged(customerState.getCustomerId(), newName));
    }

    public void changeEmail(String newEmail) {
        derivedEvents.add(new CustomerEmailChanged(customerState.getCustomerId(), newEmail));
    }

    public List<Event> getDerivedEvents() {
        return derivedEvents;
    }

}
