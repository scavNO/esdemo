package com.devbugger.esdemo.customer;

public class CustomerState {

    private CustomerId customerId;

    public CustomerState(CustomerId customerId) {
        this.customerId = customerId;
    }

    public CustomerId getCustomerId() {
        return customerId;
    }
}