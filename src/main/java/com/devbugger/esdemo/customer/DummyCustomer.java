package com.devbugger.esdemo.customer;

public class DummyCustomer {

    private CustomerId id;
    private String name;
    private String email;

    public DummyCustomer(CustomerId id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    public DummyCustomer(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public CustomerId getId() {
        return id;
    }

    public void setId(CustomerId id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
