package com.devbugger.esdemo.customer;

import com.devbugger.esdemo.core.AggregateId;

public class CustomerId extends AggregateId {

    public CustomerId(String id) {
        super(id);
    }

}