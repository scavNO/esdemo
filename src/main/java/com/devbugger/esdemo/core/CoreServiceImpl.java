package com.devbugger.esdemo.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;

@Service("coreService")
public class CoreServiceImpl implements CoreService {

    @Autowired
    private EventStore eventStore;

    private static final String FILE_NAME = "eventStore";

    public void serializeEventStore() {
        try(FileOutputStream fos = new FileOutputStream(FILE_NAME);
            ObjectOutputStream oos = new ObjectOutputStream(fos)) {

            oos.writeObject(eventStore.getEvents());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void initEventStore() {
        ArrayList<Event> initEvents = new ArrayList<>();

        try(FileInputStream fis = new FileInputStream(FILE_NAME);
            ObjectInputStream ois = new ObjectInputStream(fis)) {

            initEvents = (ArrayList<Event>) ois.readObject();

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        eventStore.initializeEventStore(initEvents);
    }
}
