package com.devbugger.esdemo.core;

public interface CoreService {

    public void serializeEventStore();

    public void initEventStore();
}
