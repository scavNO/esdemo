package com.devbugger.esdemo.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EventStore {

    private Map<AggregateType, List<Projection>> subscribingProjections = new HashMap<>();
    private List<Event> events = new ArrayList<>();

    /**
     * Initialize the event store with all belonging events.
     * @param initEvents is the complete list of persisted events we want back.
     */
    public void initializeEventStore(List<Event> initEvents) {
        storeEvents(initEvents);
    }

    public List<Event> getEvents() {
        return events;
    }

    public List<Event> getEventsByAggregateId(AggregateId id) {
        List<Event> aggregateEvents = new ArrayList<>();

        events.forEach((event) -> {
            if(event.getAggregateId().equals(id)) {
                aggregateEvents.add(event);
            }
        });

        return aggregateEvents;
    }

    public List<Event> getEventsByAggregateType(AggregateType aggregateType) {
        List<Event> aggregateEvents = new ArrayList<>();

        events.forEach((event) -> {
            if(event.getAggregateType().equals(aggregateType)) {
                aggregateEvents.add(event);
            }
        });

        return aggregateEvents;
    }

    public void storeEvents(List<Event> derivedEvents) {
        derivedEvents.forEach((event) -> {
            events.add(event);
            sendToSubscribingProjections(event);
        });
    }

    private void sendToSubscribingProjections(Event event) {
        subscribingProjections.get(event.getAggregateType()).forEach(
                (projection) -> projection.handleEvent(event));
    }

    public void subscribe(AggregateType aggregateType, Projection projection) {
        if(subscribingProjections.get(aggregateType) == null) {
            subscribingProjections.put(aggregateType, new ArrayList<>());
        }

        subscribingProjections.get(aggregateType).add(projection);
    }
}