package com.devbugger.esdemo.core;

public enum AggregateType {
    ACCOUNT, CUSTOMER
}
