package com.devbugger.esdemo.core;

import java.io.Serializable;
import java.util.UUID;

public abstract class AggregateId implements Serializable {
    static final long serialVersionUID = 4935804395286546781L;

    private final String aggregateId;

    protected AggregateId(String aggregateId) {
        this.aggregateId = aggregateId;
    }

    public String getAggregateId() {
        return aggregateId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AggregateId that = (AggregateId) o;

        if (!aggregateId.equals(that.aggregateId)) return false;

        return true;
    }

    public static String create() {
        return UUID.randomUUID().toString();
    }

    @Override
    public int hashCode() {
        return aggregateId.hashCode();
    }
}