package com.devbugger.esdemo.core;

import java.io.Serializable;

public abstract class Event implements Serializable {

    public abstract String getOccurred();

    public abstract AggregateId getAggregateId();

    public abstract AggregateType getAggregateType();

}