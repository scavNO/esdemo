package com.devbugger.esdemo;

/**
 * Define possible profiles for Gradle / Spring support
 * for selecting configuration and runtime properties
 * at gradle level.
 */
public class ESDemoProfile {

    /**
     * Using static dummy data generated in code.
     * This is the same data that tests will use when testing
     * serializing features of the eventStore.
     */
    public static final String DUMMY = "DUMMY";

    /**
     * Persisted eventStore serialized from disk.
     * This profile plays back the events to populate
     * the projections with up to date representations of POJO's.
     */
    public static final String PERSISTED = "PERSISTED";
}
