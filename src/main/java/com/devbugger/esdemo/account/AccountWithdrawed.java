package com.devbugger.esdemo.account;

public class AccountWithdrawed extends AccountEvent {

    private double withdraw;

    public AccountWithdrawed(AccountId accountId, double withdraw) {
        super(accountId);
        this.withdraw = withdraw;
    }

    public double getWithdraw() {
        return withdraw;
    }
}
