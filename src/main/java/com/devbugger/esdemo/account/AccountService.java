package com.devbugger.esdemo.account;

public interface AccountService {

    public void createAccount(AccountId id);

    public void deposit(AccountId id, double deposit);

    public void withdraw(AccountId id, double withdraw);

}
