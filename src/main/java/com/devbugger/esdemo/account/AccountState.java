package com.devbugger.esdemo.account;

public class AccountState {

    private AccountId accountId;

    public AccountState(AccountId accountId) {
        this.accountId = accountId;
    }

    public AccountId getAccountId() {
        return accountId;
    }
}
