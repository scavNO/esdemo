package com.devbugger.esdemo.account;

import com.devbugger.esdemo.core.AggregateId;

public class Account {

    private AggregateId aggregateId;

    private String name;

    private double balance;

    public Account(AggregateId aggregateId) {
        this.aggregateId = aggregateId;
    }

    public AggregateId getAggregateId() {
        return aggregateId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBalance() {
        return balance;
    }

    public void withdraw(double withdraw) {
        if(this.balance - withdraw > 0) {
            this.balance -= withdraw;
        }

        else {
            System.err.println("Not enough funds to withdraw");
        }
    }

    public void deposit(double deposit) {
        this.balance += deposit;
    }

    @Override
    public String toString() {
        return "Account{" +
                "aggregateId=" + aggregateId +
                ", name='" + name + '\'' +
                ", balance=" + balance +
                '}';
    }
}
