package com.devbugger.esdemo.account;

import com.devbugger.esdemo.core.Event;

import java.util.ArrayList;
import java.util.List;

public class AccountAggregate {

    private AccountState accountState;
    private List<Event> derivedEvents = new ArrayList<>();

    public AccountAggregate(List<Event> events) {
        events.forEach((event) -> {
            if(event instanceof AccountCreated) {
                accountState = new AccountState((AccountId)event.getAggregateId());
            }

            else if(event instanceof AccountDeposited) {
                accountState = new AccountState((AccountId)event.getAggregateId());
            }

            else if(event instanceof AccountWithdrawed) {
                accountState = new AccountState((AccountId)event.getAggregateId());
            }
        });
    }

    public void createAccount(AccountId id) {
        AccountCreated accountCreated = new AccountCreated(id);
        accountState = new AccountState((accountCreated).getAggregateId());
        derivedEvents.add(accountCreated);

    }

    public void deposite(double newDeposit) {
        derivedEvents.add(new AccountDeposited(accountState.getAccountId(), newDeposit));
    }

    public void withdraw(double withdrawlAmount) {
        derivedEvents.add(new AccountWithdrawed(accountState.getAccountId(), withdrawlAmount));
    }

    public List<Event> getDerivedEvents() {
        return derivedEvents;
    }

}
