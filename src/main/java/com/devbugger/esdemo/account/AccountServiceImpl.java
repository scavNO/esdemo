package com.devbugger.esdemo.account;

import com.devbugger.esdemo.core.Event;
import com.devbugger.esdemo.core.EventStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("accountService")
public class AccountServiceImpl implements AccountService {

    @Autowired
    private EventStore eventStore;

    @Override
    public void createAccount(AccountId id) {
        List<Event> events = eventStore.getEventsByAggregateId(id);
        AccountAggregate accountAggregate = new AccountAggregate(events);
        accountAggregate.createAccount(id);

        List<Event> derivedEvents = accountAggregate.getDerivedEvents();
        eventStore.storeEvents(derivedEvents);
    }

    @Override
    public void deposit(AccountId id, double deposit) {
        List<Event> events = eventStore.getEventsByAggregateId(id);
        AccountAggregate accountAggregate = new AccountAggregate(events);
        accountAggregate.deposite(deposit);

        List<Event> derivedEvents = accountAggregate.getDerivedEvents();
        eventStore.storeEvents(derivedEvents);
    }

    @Override
    public void withdraw(AccountId id, double withdraw) {
        List<Event> events = eventStore.getEventsByAggregateId(id);
        AccountAggregate accountAggregate = new AccountAggregate(events);
        accountAggregate.withdraw(withdraw);

        List<Event> derivedEvents = accountAggregate.getDerivedEvents();
        eventStore.storeEvents(derivedEvents);
    }
}