package com.devbugger.esdemo.account;

import com.devbugger.esdemo.core.AggregateId;

public class AccountId extends AggregateId {

    public AccountId(String id) {
        super(id);
    }
}
