package com.devbugger.esdemo.account;

public class AccountDeposited extends AccountEvent {

    private double despoit;

    public AccountDeposited(AccountId accountId, double despoit) {
        super(accountId);
        this.despoit = despoit;
    }

    public double getDeposit() {
        return despoit;
    }
}
