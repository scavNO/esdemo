package com.devbugger.esdemo.account;

public class AccountCreated extends AccountEvent {

    public AccountCreated(AccountId accountId) {
        super(accountId);
    }
}
