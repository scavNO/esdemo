package com.devbugger.esdemo.account;

import com.devbugger.esdemo.core.AggregateType;
import com.devbugger.esdemo.core.Event;

import java.time.Instant;

public class AccountEvent extends Event {

    private final AccountId accountId;

    private final String occurred;

    public AccountEvent(AccountId accountId) {
        this.accountId = accountId;
        this.occurred = Instant.now().toString();
    }

    @Override
    public String getOccurred() {
        return occurred;
    }

    @Override
    public AccountId getAggregateId() {
        return accountId;
    }

    @Override
    public AggregateType getAggregateType() {
        return AggregateType.ACCOUNT;
    }
}
