package com.devbugger.esdemo.account;

import com.devbugger.esdemo.core.*;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AccountProjection extends Projection {


    private Map<AggregateId, Account> accounts = new HashMap<>();

    public AccountProjection(EventStore eventStore) {
        super(eventStore);
    }

    @Override
    protected AggregateType getAggregateType() {
        return AggregateType.ACCOUNT;
    }

    @Override
    public void handleEvent(Event event) {
        if(event instanceof AccountCreated) {
            createAccount((AccountCreated) event);
        }

        else if(event instanceof AccountDeposited) {
            deposit((AccountDeposited) event);
        }

        else if(event instanceof AccountWithdrawed) {
            withdrawl((AccountWithdrawed) event);
        }
    }

    private void createAccount(AccountCreated event) {
        if(accounts.containsKey(event.getAggregateId())) {
            System.err.println("Already contains this account!");
        }

        else {
            accounts.put(event.getAggregateId(), new Account(event.getAggregateId()));
        }
    }

    private void deposit(AccountDeposited event) {
        Account account = accounts.get(event.getAggregateId());
        account.deposit(event.getDeposit());
        accounts.put(event.getAggregateId(), account);
    }

    private void withdrawl(AccountWithdrawed event) {
        Account account = accounts.get(event.getAggregateId());
        account.withdraw(event.getWithdraw());
        accounts.put(event.getAggregateId(), account);
    }

    public Account getAccount(AccountId accountId) {
        return accounts.get(accountId);
    }

    public List<Account> getAllAccounts() {
        return new ArrayList<>(accounts.values());
    }
}
