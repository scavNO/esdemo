package com.devbugger.esdemo;

import com.devbugger.esdemo.core.CoreService;
import com.devbugger.esdemo.core.EventStore;
import com.devbugger.esdemo.customer.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class ESDemo extends SpringBootServletInitializer {
    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(ESDemo.class);
    }

    @Autowired
    private CustomerService customerService;

    @Autowired
    private CoreService coreService;

    @Bean
    public MappingJackson2HttpMessageConverter messageConverter() {
        MappingJackson2HttpMessageConverter messageConverter =
                new MappingJackson2HttpMessageConverter();

        messageConverter.setPrettyPrint(true);

        return messageConverter;
    }

    @Bean
    public EventStore eventStore() {
        return new EventStore();
    }

    @Bean
    @Profile(ESDemoProfile.DUMMY)
    public SetupESDemo setupESDemoDummy() {
        SetupESDemo setupESDemo = new SetupESDemo(coreService, customerService);
        setupESDemo.initializeDummeEventStore();
        return setupESDemo;
    }

    @Bean
    @Profile(ESDemoProfile.PERSISTED)
    public SetupESDemo setupESDemoPersisted() {
        SetupESDemo setupESDemo = new SetupESDemo(coreService, customerService);
        setupESDemo.initializePersistedEventStore();
        return setupESDemo;
    }
}