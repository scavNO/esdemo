package com.devbugger.esdemo;

import com.devbugger.esdemo.core.CoreService;
import com.devbugger.esdemo.customer.CustomerId;
import com.devbugger.esdemo.customer.CustomerService;

import java.util.ArrayList;
import java.util.List;

public class SetupESDemo {

    private CoreService coreService;
    private CustomerService customerService;

    public SetupESDemo(CoreService coreService, CustomerService customerService) {
        this.coreService = coreService;
        this.customerService = customerService;
    }

    public void initializeDummeEventStore() {
        DUMMY_CUSTOMERS.forEach((c) -> customerService.createCustomer((c)));

        customerService.changeEmail(DUMMY_CUSTOMERS.get(0), "dagherad@gmail.com");
        customerService.changeName(DUMMY_CUSTOMERS.get(0), "Dag Heradstveit");

        customerService.changeEmail(DUMMY_CUSTOMERS.get(1), "frank@gmail.com");
        customerService.changeName(DUMMY_CUSTOMERS.get(1), "Frank Franksen");

        customerService.changeEmail(DUMMY_CUSTOMERS.get(2), "petter@gmail.com");
        customerService.changeName(DUMMY_CUSTOMERS.get(2), "Petter Fletter");

        customerService.changeEmail(DUMMY_CUSTOMERS.get(3), "sverre@gmail.com");
        customerService.changeName(DUMMY_CUSTOMERS.get(3), "Sverre Herresverre");
    }

    public void initializePersistedEventStore() {
        coreService.initEventStore();
    }

    private final static List<CustomerId> DUMMY_CUSTOMERS = new ArrayList<>();
    static {
        DUMMY_CUSTOMERS.add(new CustomerId("fd2bf74a-53e6-49d7-86f5-d644d44f0c7b"));
        DUMMY_CUSTOMERS.add(new CustomerId("79f75fd0-7630-4c27-a5b2-5521e734a910"));
        DUMMY_CUSTOMERS.add(new CustomerId("cf2f1c7e-11b6-4f07-a5e3-91d86c595f7d"));
        DUMMY_CUSTOMERS.add(new CustomerId("47d58bf3-9d54-4ce1-908b-ac4afa0405a6"));
        DUMMY_CUSTOMERS.add(new CustomerId("5754c36b-88fa-4ec0-85d2-b0962ccb8a5c"));
        DUMMY_CUSTOMERS.add(new CustomerId("55cc1575-961c-4e7d-b182-3da2ad5cf166"));
    }
}